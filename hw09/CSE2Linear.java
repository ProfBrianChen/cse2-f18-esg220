import java.util.Arrays;

import java.util.Scanner;

public class CSE2Linear {

   private static int BigSize = 5;

   public static void main(String[] args) {
       int[] Score = Grades();
       Arrays.sort(Score);
     
   }

   private static int[] Grades() {
       int[] Score = new int[BigSize];
       Scanner scanner = new Scanner(System.in);
       for (int i = 0; i < BigSize; i++) {
           System.out.print("Grade: " + (i + 1) +"/"+ BigSize + " :" );
           Score[i] = scanner.nextInt();
           System.out.print(System.lineSeparator());
       
       System.out.println("Grades entered:");
       for(int j=0;i<BigSize;j++){
           System.out.print(Score[j]+" ");
       }
       System.out.println();
       
   }
   return Score;  
}
}