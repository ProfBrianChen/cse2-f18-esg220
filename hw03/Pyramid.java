import java.util.Scanner;
// establishes the scanner
// 
//
public class Pyramid{
    			// main method required for every Java program
   			public static void main(String[] args) {
Scanner myScanner = new Scanner( System.in );
System.out.print("The square side of the pyramid is ");
double pyramidSideLength = myScanner.nextInt();
System.out.print("The height of the pyramid is ");
double pyramidHeight = myScanner.nextInt();
double square = pyramidSideLength * pyramidSideLength;
double vol;          
vol =square * pyramidHeight / 3;
System.out.print("The volume of the pyramid is " + vol );
        }
}