import java.util.Scanner;
// establishes the scanner
//
//
public class Convert{
    			// main method required for every Java program
   			public static void main(String[] args) {
Scanner myScanner = new Scanner( System.in );
System.out.print("Enter the affected area in acres:");
double acres = myScanner.nextInt();
System.out.print("Enter the rainfall in the affected area:");
double inchesRainfall = myScanner.nextInt();
double squareInches = acres * 6.273e6;
double cubicInches = squareInches * 216000;
double gallons = cubicInches * 0.004329;
double cubicMiles = gallons * 9.08169e-13;
System.out.print(" Cubic Miles = " + cubicMiles);
        }
}