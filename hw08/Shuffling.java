// Elliot Gilliam HW 8
import java.util.Scanner;
import java.util.Random;
public class Shuffling{ 
public static void main(String[] args) { 
Scanner scan = new Scanner(System.in); 
 //suits club, heart, spade or diamond 
 String[] suitNames={"C","H","S","D"};    
String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
String[] cards = new String[52]; 
String[] hand = new String[5]; 
int numCards = 5; 
int again = 1; 
int index = 51;
for (int i=0; i<52; i++){ 
  cards[i]=rankNames[i%13]+suitNames[i/13]; 
  System.out.print(cards[i]+" "); 
} 
System.out.println();
printArray(cards); 
shuffle(cards); 
printArray(cards); 
while(again == 1){ 
   hand = getHand(cards,index,numCards); 
   printArray(hand);
   index = index - numCards;
   System.out.println("Enter a 1 if you want another hand drawn"); 
   again = scan.nextInt(); 
}
  } 

public static void printArray( String[] list  ){
  //prints the list of cards
  for(int i =0 ; i<list.length ; i++){
    
    System.out.print(list[i]+ " ");
  }
}
 


public static String[] getHand(String[] hand, int index , int numCards ){
  // Generates the hand properly
  String[] list = new String[numCards];
  int handCounter = 0;
  int currentCard = index;
  if (currentCard < 0) {
    currentCard = 52 + currentCard;
   }
  for (int i = 0; i < numCards; i++) {
    list[i] = hand[currentCard];
    currentCard++;
    if (currentCard == 52) {
        currentCard = 0;
     }
  }
  return list;
}
  public static void shuffle(String[] list){
    // shuffles the cards
    int swapCounter = 75;
    for (int i = 0; i < swapCounter; i++) {
      Random rand = new Random();
      int random = rand.nextInt(51);
      String temp = list[random];
      list[random] = list[0];
      list[0] = temp;
    }
  }
    
}

