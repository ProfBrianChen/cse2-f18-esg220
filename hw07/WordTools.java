// Elliot G WordTools
import java.util.Scanner;
public class WordTools {
// Intro
// First Program  
public static int getNumOfWords(final String StringAlpha) {
int incrementor = 1;
int i = 0;
   
for (i = 1; i < StringAlpha.length(); ++i) {
if ( (Character.isWhitespace(StringAlpha.charAt(i)) == true) &&
(Character.isWhitespace(StringAlpha.charAt(i - 1))) == false) {
++incrementor;
}
}
  
  
return incrementor;
  
}
// Second Method  
public static int getNumOfNonWSCharacters(final String StringAlpha) {
int incrementor = 0;
int i = 0;
  

for(i=0; i < StringAlpha.length(); i++){
if (!Character.isWhitespace(StringAlpha.charAt(i))){
++incrementor;
}
}
return incrementor;
}
//Third Method
public static int findText(final String Locate, String StringAlpha) {
int incrementor = 0;
int file = 0;
  

do {
file = StringAlpha.indexOf(Locate);
  
if (file != -1)
incrementor++;
StringAlpha = StringAlpha.substring(file + Locate.length() - 1);
}while(file != -1); 
  
return incrementor;
}
// FourthMethod
public static String replaceExclamation(String StringAlpha) {
return StringAlpha.replaceAll("!", ".");
}
// FifthMethod
public static String shortenSpace(String StringAlpha) {

String spaceFirst = " ";
String spaceSecond = spaceFirst + spaceFirst;
  
while (StringAlpha.indexOf(spaceSecond) != -1) {

StringAlpha=StringAlpha.replace(spaceSecond, spaceFirst);
}
return StringAlpha;
}
// SixthMethod
public static char printStatements(Scanner scnr) {
char menuLists = ' ';
  
System.out.println("\nMENU");
System.out.println("c - Number of non-whitespace characters");
System.out.println("w - Number of words");
System.out.println("f - Find text");
System.out.println("r - Replace all !'s");
System.out.println("s - Shorten spaces");
System.out.println("q - Quit");
System.out.print("\n");


while (menuLists != 'c' && menuLists != 'w' && menuLists != 'f' &&
menuLists != 'r' && menuLists != 's' && menuLists != 'o' &&
menuLists != 'q') {
System.out.println( "Choose an option:");
menuLists = scnr.nextLine().charAt(0);
}
  
return menuLists;
}
//Main
public static void main(String[] args) {
Scanner scnr = new Scanner(System.in);
String StringInput = "";
char menuSelection = ' ';
String Locate = "";
  


System.out.println("Enter a sample text:");
StringInput = scnr.nextLine();
  
System.out.println("\nYou entered: " + StringInput);
  
  

while (menuSelection != 'q'){
menuSelection = printStatements(scnr);

if (menuSelection == 'c') {
System.out.println("Number of non-whitespace characters: " + getNumOfNonWSCharacters(StringInput));

}


else if (menuSelection == 'w') {
System.out.println("Number of words: " + getNumOfWords(StringInput));

}

else if (menuSelection == 'f'){
System.out.println("Enter a word or phrase to be found:");
Locate = scnr.nextLine();
System.out.println("\"" + Locate + "\" instances: " + findText(Locate, StringInput));
  
}

else if (menuSelection == 'r') {
System.out.println("Edited text: " + replaceExclamation(StringInput));
}

else if (menuSelection == 's') {
System.out.println("Edited text: " + shortenSpace(StringInput));

  
}
  
}
  
}
}